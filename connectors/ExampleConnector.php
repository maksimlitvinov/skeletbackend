<?php
/**
 * Доступен DI $this->di контейнер и $this->data
 *
 * Для возврата результата используется метод output
 */

namespace Api;

use Kefir\Services\Connector\CustomConnector;

class ExampleConnector extends CustomConnector
{
    protected $module = 'ExampleModule';

    public function getExample()
    {
        parse_str($this->data['form'], $form);

        $module = $this->di->get($this->module);

        $class = $this->prepareAction($this->data['action'], 'class');

        $action = $this->prepareAction($this->data['action'], 'action');

        $output = $module->$action([
            'class' => $class,
            'action' => $action,
            'data' => $form
        ]);

        $this->output(true, '', $output);
    }
}