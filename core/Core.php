<?php

namespace Kefir;

use Kefir\DI\DI;
use Kefir\Services\Http\Request;
use Kefir\Services\Config\Config;
use Kefir\Services\Database\Connection;
use Kefir\Services\Router\Router;
use Kefir\Helper\Common;
use Kefir\Services\Templates\Template;
use Kefir\Services\Thumb\Thumb;
use Kefir\Services\Files\Files;
use Kefir\Services\Connector\Connector;
use Kefir\Services\Cron\CronConnector as Cron;

class Core
{
    /**
     * @var DI
     */
    public $di;
    /**
     * @var Request
     */
    public $req;
    /**
     * @var Connection
     */
    public $db;
    /**
     * @var Router
     */
    public $router;
    /**
     * @var Template
     */
    public $template;
    /**
     * @var Config
     */
    public $config;
    /**
     * @var Thumb
     */
    public $thumb;
    /**
     * @var Files
     */
    public $files;
    /**
     * @var Cron
     */
    public $cron;
    /**
     * @var bool
     */
    private $is_ajax = false;


    /**
     * Core constructor.
     */
    public function __construct()
    {
        session_start();

        $this->di = new DI();

        $this->req = new Request();
        $this->di->set('req', $this->req);

        $this->config = new Config($this->di);
        $this->di->set('config', $this->config);
        $this->config->getMainConfig();

        $this->thumb = new Thumb();
        $this->di->set('thumb', $this->thumb);

        $this->files = new Files($this->di);
        $this->di->set('files', $this->files);

        $this->db = new Connection($this->di);
        $this->di->set('db', $this->db);

        $this->router = new Router($this->di);
        $this->di->set('router', $this->router);

        $this->template = new Template();
        $this->di->set('template', $this->template);

        $this->cron = new Cron($this->di);
        $this->di->set('cron', $this->cron);

    }

    /**
     * Инициализация приложения
     */
    public function init()
    {
        $this->is_ajax = $this->isAjax();

        /**
         * Редирект что бы на конце был /
         */
        $prepare_url = Common::getPatchUrl($this->req, $this->is_ajax);

        /**
         * Получение роутов
         */
        $this->getRoutes();

        /**
         * Загрузка пользовательских модулей.
         * Выполняется тут а не в конструкторе потому что возможна загрузка роутов.
         */
        $this->getUsersModules('Modules');

        $routerDispatch = $this->router->dispatch($this->req->server['REQUEST_METHOD'], $prepare_url);

        /**
         * Если пришел Ajax Запрос
         */
        if ($this->is_ajax) {
            $connector = new Connector($this->di, $routerDispatch);
            $connector->initialization();
            exit;
        }

        /**
         * Если не существует роут.
         */
        if ($routerDispatch == null) {
            $this->router->errorRedirect('404', 404);
        }

        /**
         * Если кефирчик на обслуживании отдаем 503-ю
         */
        if (!$this->checkKefirStatus()) {
            $this->router->errorRedirect('503', 503);
        }

        list($class, $action) = explode(':', $routerDispatch->getController(), 2);

        $envController = '\\'. ENV .'\\Controllers\\' . $class;
        $coreController = '\Kefir\Controllers\\' . $class;

        /**
         * Проверка существования запроса.
         */
        if (class_exists($envController)) {
            $controller = $envController;
        } elseif (class_exists($coreController)) {
            $controller = $coreController;
        } else {
            $controller = '\Kefir\Controllers\Error\Page404Controller';
        }

        /**
         * Получение параметров
         */
        $parameters = $routerDispatch->getParameters();

        call_user_func_array([new $controller($this->di), $action], $parameters);
    }

    /**
     * Получение роутов: Системные и роуты окружений
     */
    public function getRoutes()
    {
        require_once $this->req->server['DOCUMENT_ROOT'] . '/core/Services/Router/Route.php';

        $routes = '\\' . ENV . '\\Routes\\Routes';
        $routes = new $routes($this->di);
        $routes->getEnvRoutes();
    }

    /**
     * @param $key
     * @return null
     */
    private function checkKefirStatus()
    {
        $otherConfig = $this->config->getConfig('Other');

        if (ENV == 'App' && $otherConfig['site_enable']) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function isAjax()
    {
        $this->is_ajax = isset($this->req->server['HTTP_X_REQUESTED_WITH']) && $this->req->server['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
        return $this->is_ajax;
    }

    /**
     * Метод загрузки пользовательских модулей
     * @param $namespace
     */
    public function getUsersModules($namespace)
    {
        $path = $this->req->server['DOCUMENT_ROOT'] . '/modules/';
        $listDirs = array_diff(scandir($path), ['.','..']);
        if (empty($listDirs)) {return;}
        foreach ($listDirs as $listDir) {
            $class = '\\'.$namespace.'\\'.$listDir.'\\'.$listDir;
            if (!class_exists($class)) {break;}
            $this->di->set($listDir, new $class($this->di));
        }
    }
}