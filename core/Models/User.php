<?php

namespace Kefir\Models;

class User extends BaseModel
{
    protected $table = 'users';
    protected $fillable = [
        'login','name','surname','patronymic','phone','email','images','appointment_id','pass','salt',
    ];
    protected $hidden = ['pass','salt'];
    protected $dates = ['deleted_at'];

    /*public function doljnost()
    {
        return $this->belongsTo('\Kefir\Models\Appointment');
    }*/
}