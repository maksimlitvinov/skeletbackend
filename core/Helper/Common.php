<?php

namespace Kefir\Helper;

class Common
{

    public static function getPatchUrl($req) {
        $pathUrl = $req->server['REQUEST_URI'];

        if($position = strpos($pathUrl, '?')) {
            $pathUrl = substr($pathUrl, 0, $position);
        }

        $arrUrl = explode('/', $pathUrl);
        if (!empty($arrUrl[count($arrUrl) - 1])) {
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $pathUrl ."/");
            exit();
        }

        return $pathUrl;
    }
}