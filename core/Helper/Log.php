<?php

namespace Kefir\Helper;

class Log
{
    public static function log($message, $log = 'base', $level = E_USER_ERROR)
    {
        $tolog = '';

        $fp = fopen(PROJECT_LOG_PATH . $log . '.log', 'a+');

        if (!is_string($message)) {
            $message = print_r($message, true);
        }
        $tolog .= '[' . date('D M d H:i:s Y', time()) . '] ';
        $tolog .= $message . " \n";

        fwrite($fp, $tolog);

        fclose($fp);

    }
}