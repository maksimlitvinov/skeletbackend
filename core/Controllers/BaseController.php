<?php

namespace Kefir\Controllers;

use Kefir\DI\DI;

abstract class BaseController
{

    use CommonTrait;

    /**
     * @var DI
     */
    public $di;
    /**
     * @var \Kefir\Services\Http\Request
     */
    public $req;
    /**
     * @var \Kefir\Services\Config\Config
     */
    public $config;
    /**
     * @var \Kefir\Services\Database\Connection
     */
    public $db;
    /**
     * @var \Kefir\Services\Router\Router
     */
    public $router;
    /**
     * @var \Kefir\Services\Templates\Template
     */
    public $template;
    /**
     * @var \Kefir\Services\Thumb\Thumb
     */
    public $thumb;
    /**
     * @var \Kefir\Services\Files\Files
     */
    public $files;

    /**
     * @var string
     */
    public $pagetitle = PROJECT_NAME;
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var string
     */
    public $action = '';
    /**
     * @var array
     */
    public $deleted = [];
    /**
     * @var array
     */
    public $currentUser = [];

    /**
     * BaseController constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;

        $this->req      = $this->di->get('req');
        $this->config   = $this->di->get('config');
        $this->db       = $this->di->get('db');
        $this->router   = $this->di->get('router');
        $this->template = $this->di->get('template');
        $this->thumb    = $this->di->get('thumb');
        $this->files    = $this->di->get('files');
    }

    /**
     * @param string $url
     */
    public static function Redirect($url = '/')
    {
        header("Location: {$url}");
        exit();
    }
}