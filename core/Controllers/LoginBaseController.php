<?php

namespace Kefir\Controllers;

use Kefir\DI\DI;
use Kefir\Helper\Log;
use Kefir\Helper\Session;

abstract class LoginBaseController
{

    use CommonTrait;

    /** @var DI */
    public $di;

    /** @var \Kefir\Services\Templates\Template */
    public $template;

    /** @var \Kefir\Services\Http\Request */
    public $req;

    public $pagetitle = 'Авторизация на сайте';

    public $action = '';

    public $data = [];

    public $deleted = [];

    public $currentUser = [];


    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->template = $this->di->get('template');
        $this->req = $this->di->get('req');
    }

    abstract public function index();

    abstract public function login();

    abstract public function logout();

    /**
     * @param $step
     * @param string $message
     * @param string $privatMes
     */
    public function errorLogin($step, $message = '', $privatMes = '')
    {
        Log::log($step . ': ' . $privatMes, 'login');
        Session::set('messages', $message);
        self::Redirect();
    }

    /**
     * @param string $url
     */
    public static function Redirect($url = '/')
    {
        header("Location: {$url}");
        exit;
    }

    /**
     * @param $user
     * @return bool
     */
    public static function checkErrorLogin($user)
    {
        if ((int) $user->login_error >= 3) {
            return true;
        }

        return false;
    }
}