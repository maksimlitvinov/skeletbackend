<?php

namespace Kefir\Controllers;

use Kefir\Helper\Session;

trait CommonTrait
{

    protected $prepareDate = [];

    /**
     * Подготовка данных для передачи в шаблон
     * @param string $key
     * @return array
     */
    public function getDataToTemplate($key = 'data')
    {

        $this->prepareDate['assets_url'] = PROJECT_ASSETS_URL;
        $this->prepareDate['assets_path'] = PROJECT_ASSETS_PATH;
        $this->prepareDate['site_url'] = PROJECT_SITE_URL;
        $this->prepareDate['site_name'] = PROJECT_NAME;
        $this->prepareDate['site_name_lower'] = PROJECT_NAME_LOWER;

        $this->prepareDate['pagetitle'] = $this->pagetitle;

        // Действия для подстановки в форму
        if ($this->action) {
            $this->prepareDate['action'] = $this->action;
        }

        // Основные данные
        if ($this->data) {
            $this->prepareDate[$key] = $this->data;
        }

        // Для отображения удаленных позиций
        if ($this->deleted) {
            $this->prepareDate['deleted'] = $this->deleted;
        }

        // Авторизованный пользователь
        if ($this->currentUser) {
            if ($this->currentUser->pass) {unset($this->currentUser->pass);}
            if ($this->currentUser->salt) {unset($this->currentUser->salt);}
            if ($this->currentUser->doljnost) {unset($this->currentUser->doljnost);}
            $this->prepareDate['currentUser'] = $this->currentUser;
            $this->prepareDate['currentUser']['openVisoviCount'] = $this->currentUserVisovi;
            $this->prepareDate['currentUser']['openWorkCount'] = $this->currentUserWorkshop;
        }

        // Сообщения
        if (Session::get('messages')) {
            $this->prepareDate['messages'] = Session::get('messages');
            Session::delete('messages');
        }

        return $this->prepareDate;
    }
}