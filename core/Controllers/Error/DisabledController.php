<?php

namespace Kefir\Controllers\Error;

use Kefir\Controllers\CommonTrait;
use Kefir\DI\DI;

class DisabledController
{
    use CommonTrait;
    /**
     * @var DI
     */
    public $di;
    /**
     * @var \Kefir\Services\Templates\Template
     */
    public $template;
    /**
     * @var string
     */
    public $pagetitle = 'Техническое обслуживание сайта | WebPortal';
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var array
     */
    public $deleted = [];
    /**
     * @var string
     */
    public $action = '';
    /**
     * @var array
     */
    public $currentUser = [];

    /**
     * DisabledController constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->template = $this->di->get('template');
    }

    /**
     * Отдаем 503-ю ошибку и говорим что сайт временно недоступен
     */
    public function index()
    {
        header("HTTP/1.1 503 Service Unavailable");
        header("Status: 503 Service Unavailable");

        echo $this->template->render('Error/disabled', $this->getDataToTemplate(), true);
    }

}