<?php

namespace Kefir\Controllers;

use Kefir\Services\User;
use Kefir\Helper\Log;
use Kefir\Helper\Session;
use Kefir\Services\Auth\Auth;


class UserController extends BaseController
{
    /**
     * @var string
     */
    public $pagetitle = 'Личный кабинет';

    /**
     * Основной метод настроек
     */
    public function index()
    {
        $id = $this->currentUser->id;
        $this->data = self::getUser($id);
        echo $this->template->render('Users/index', $this->getDataToTemplate('user'));
    }

    /**
     * Форма редактирования данных в личном кабинете
     */
    public function edit()
    {
        $id = $this->currentUser->id;
        $this->data = self::getUser($id);
        $this->action = '/user/update/';
        $this->pagetitle = 'Редактирование пользователя';
        $this->data['backLink'] = '/user/';

        echo $this->template->render('Users/edit', $this->getDataToTemplate('user'));
    }

    public function create()
    {
        // Добавление пользователя реализовано в SettingsController
    }

    public function delete($id)
    {
        // Удаление пользователя реализовано в SettingsController
    }

    public function add()
    {
        // Добавление пользователя реализовано в SettingsController
    }

    /**
     * Обновление данных в личном кабинете
     */
    public function update()
    {
        $params = $this->req->getPost();
        if (empty($params['pass'])) {
            unset($params['pass']);
            unset($params['confirmPass']);
        } else {
            if ($params['pass'] != $params['confirmPass']) {
                Log::log('UserUpdate: Пароли не совпадают. ' . $params['login']);
                Session::set('messages', 'Пароли не совпадают');
                self::Redirect('/user/edit/');
            }
            unset($params['confirmPass']);
            $params['salt'] = Auth::salt();
            $params['pass'] = Auth::encryptPassword($params['pass'], $params['salt']);
        }

        $file = $this->req->files['attachmentName'];
        if ($file['name']) {
            $params['image'] = $this->files->getUserFilesPath($params['id'], 'avatar', true) . $file['name'];

            $response = $this->files->uploadFile($this->req->files);

            if ($response) {
                if ($response['error']) {
                    self::Redirect('/user/');
                }
            }
        }

        self::uib($params, '/user/');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getUsers()
    {
        return User::with('doljnost')->withTrashed()->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getUser($id)
    {
        $user = User::with('doljnost')->where('id', $id)->get();
        return $user[0];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getEngeneer()
    {
        $sacsess_doljnost = ['2','3'];
        return User::with('doljnost')->whereIn('doljnost_id', $sacsess_doljnost)->get();
    }

    /**
     * @param $login
     * @return \___PHPSTORM_HELPERS\static|mixed
     */
    public static function getUserForLogin($login)
    {
        $user = User::with('doljnost')->where('login', '=', $login)->get();
        return $user[0];
    }

    /**
     * @return mixed
     */
    public static function getPartners()
    {
        return User::where('partner_id', '!=', 'null')->get();

    }

    /**
     * UIB - Update In Base
     * @param array $params
     * @param string $url
     */
    public static function uib(array $params = [], $url = '', $redirect = true)
    {
        $user = User::find($params['id']);
        foreach($params as $key => $value) {
            $user->{$key} = $value;
        }
        $updated = $user->save();
        if ($redirect) {
            self::Redirect($url);
        }
    }

    /**
     * IIB - Insert In Base
     * @param array $params
     * @param string $url
     * @param bool $redirect
     * @return mixed
     */
    public static function iib($params = [], $url = '', $redirect = true)
    {
        $user = User::create($params);

        if ($redirect) {
            self::Redirect($url);
        } else {
            return $user;
        }
    }

    /**
     * @return int
     */
    public static function generatePartnerId()
    {
        $user = User::select('partner_id')->where('partner_id', '!=', NULL)->orderBy('partner_id', 'desc')->limit(1)->get();
        if ($user->count() == 0) {
            return 1;
        }

        return (int) $user[0]->partner_id + 1;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function updatePartnerId($id)
    {
        $user = User::select('partner_id')->where('id', $id)->get();
        return $user[0]->partner_id;
    }
}