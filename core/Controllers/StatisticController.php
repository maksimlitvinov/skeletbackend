<?php

namespace Kefir\Controllers;

use Kefir\Services\Visov;
use Kefir\Services\WorkShop;

class StatisticController extends BaseController
{

    public $pagetitle = 'Статистика';

    public function index()
    {
        $this->data['visovi'] = Visov::with('partner', 'status', 'firm')
                                        ->where('engineer_id', $this->currentUser->id)
                                        ->where('status_id', 1)
                                        ->orderBy('status_id', 'asc')
                                        ->groupBy('id')
                                        ->get()->toArray();
        $this->data['visovi_count'] = Visov::where('engineer_id', $this->currentUser->id)->get()->count();

        $this->data['workshop'] = [];

        echo $this->template->render('Statistic/index', $this->getDataToTemplate());
    }
}