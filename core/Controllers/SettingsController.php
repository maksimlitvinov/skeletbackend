<?php

namespace Kefir\Controllers;

use Kefir\Controllers\UserController as User;
use Kefir\Helper\Log;
use Kefir\Helper\Session;
use Kefir\Services\Auth\Auth;

class SettingsController extends BaseController
{

    public $pagetitle = 'Настройки';

    public $data = [];

    public $user;

    public $deleted;

    public $action;

    public function index()
    {
        $this->data['users'] = User::getUsers();
        echo $this->template->render('Settings/index', $this->getDataToTemplate('settings'));
    }

    public function createUser()
    {
        $this->pagetitle = 'Добавление пользователя';
        $this->action = '/settings/users/add/';
        $this->data['backLink'] = '/settings/';
        echo $this->template->render('Users/create.tpl', $this->getDataToTemplate('user'));
    }

    /**
     * @param $id
     */
    public function editUser($id)
    {
        $this->pagetitle = 'Обновление пользователя';
        $this->action = '/settings/users/update/';
        $this->data = User::getUser($id);
        $this->data['backLink'] = '/settings/';
        echo $this->template->render('Users/edit.tpl', $this->getDataToTemplate('user'));
    }

    public function deleteUser($id)
    {
        var_dump($id);exit;
    }

    public function addUser()
    {
        $params = $this->req->getPost();
        $file = $this->req->files['attachmentName'];

        if (empty($params['pass']) || empty($params['confirmPass'])) {
            Log::log('SettingsUsersAdd: Не заполнен пароль или подтверждение пароля. Login: ' . $params['login']);
            Session::set('messages', 'Не заполнен пароль или подтверждение пароля.');
            self::Redirect('/settings/users/create/');
        }

        if ($params['pass'] != $params['confirmPass']) {
            Log::log('SettingsUsersAdd: Пароли не совпадают. Login: ' . $params['login']);
            Session::set('messages', 'Пароли не совпадают.');
            self::Redirect('/settings/users/create/');
        }

        // Является ли партнером?
        if (isset($params['partner_id']) && !empty($params['partner_id'])) {
            $params['partner_id'] = User::generatePartnerId();
        }

        $params['salt'] = Auth::salt();
        $params['pass'] = Auth::encryptPassword($params['pass'], $params['salt']);

        $newUser = User::iib($params, '', false);

        if ($file['name']) {
            $params = [];
            $params['id'] = $newUser->id;
            $params['image'] = $this->files->getUserFilesPath($params['id'], 'avatar', true) . $file['name'];

            $response = $this->files->uploadFile($this->req->files);

            if ($response) {
                if ($response['error']) {
                    Log::log('SettingsUsersAdd: Не удалось обработать фотографию. ID: ' . $params['login']);
                    Session::set('messages', 'Не удалось обработать фотографию. Попробуйте еще раз.');
                    self::Redirect('/settings/users/edit/' . $params['id']);
                }
            }

            User::uib($params, '/settings/users/edit/' . $params['id']);
        }

        self::Redirect('/settings/users/edit/' . $newUser->id);

    }

    public function updateUser()
    {
        $params = $this->req->getPost();
        if (empty($params['pass'])) {
            unset($params['pass']);
            unset($params['confirmPass']);
        } else {
            if ($params['pass'] != $params['confirmPass']) {
                Log::log('SettingsUsersUpdate: Пароли не совпадают. Login: ' . $params['login']);
                Session::set('messages', 'Пароли не совпадают.');
                self::Redirect('/settings/');
            }
            unset($params['confirmPass']);
            $params['salt'] = Auth::salt();
            $params['pass'] = Auth::encryptPassword($params['pass'], $params['salt']);
        }

        // Является ли партнером?
        if (!isset($params['partner_id'])) {
            $params['partner_id'] = null;
        } else {
            $res = User::updatePartnerId($params['id']);
            if ($res) {
                $params['partner_id'] = $res;
            } else {
                $params['partner_id'] = User::generatePartnerId();
            }
        }

        $file = $this->req->files['attachmentName'];
        if ($file['name']) {
            $params['image'] = $this->files->getUserFilesPath($params['id'], 'avatar', true) . $file['name'];

            $response = $this->files->uploadFile($this->req->files);

            if ($response) {
                if ($response['error']) {
                    self::Redirect('/settings/');
                }
            }
        }

        User::uib($params, '/settings/');
    }
}