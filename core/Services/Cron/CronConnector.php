<?php

namespace Kefir\Services\Cron;

use Kefir\DI\DI;

class CronConnector
{
    /**
     * @var DI
     */
    protected $di;

    /**
     * CronConnector constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
    }
}