<?php

namespace Kefir\Services\Http;

class Request
{

    public $post = [];

    public $get = [];

    public $request = [];

    public $files = [];

    public $cookie = [];

    public $server = [];

    public function __construct()
    {
        $this->post = $this->getPost();
        $this->get = $_GET;
        $this->request = $_REQUEST;
        $this->files = $_FILES;
        $this->cookie = $_COOKIE;
        $this->server = $_SERVER;
    }

    public function getPost()
    {
        if (empty($_POST)) {return NULL;}
        $cleanPost = [];
        foreach($_POST as $key => $value) {
            $cleanPost[$key] = htmlspecialchars($value, ENT_QUOTES);
        }
        return $cleanPost;
    }
}