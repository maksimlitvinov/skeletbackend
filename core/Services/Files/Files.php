<?php

namespace Kefir\Services\Files;

use Kefir\DI\DI;
use Kefir\Helper\Log;

class Files
{
    /**
     * @var DI
     */
    public $di;
    /**
     * @var \Kefir\Services\Thumb\Thumb
     */
    public $thumbs;
    /**
     * Путь для генерации превью
     * @var string
     */
    public $path = '';

    /**
     * Files constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->thumbs = $this->di->get('thumb');
    }

    /**
     * @param $id
     * @param null $razdel
     * @param bool $setPath
     * @return string
     */
    public function getUserFilesPath($id, $razdel = NULL, $setPath = false)
    {

        if ($razdel) {
            $razdel = '/' . $razdel . '/';
        } else {
            $razdel = '/';
        }

        $path = PROJECT_ASSETS_PATH . 'uploads/Users/' . $id . $razdel;

        $url = PROJECT_ASSETS_URL . 'uploads/Users/' . $id . $razdel;

        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        if ($setPath) {
            $this->path = $path;
        }

        return $url;
    }

    public function uploadFile(array $file)
    {

        if (empty($this->path)) {
            Log::log('Thumb: Не указан путь для генерации превью');
            return 'Не указан путь';
        }

        foreach ($file as $item) {

            move_uploaded_file($item['tmp_name'], $this->path . $item['name']);

            return $this->thumbs->optimized($this->path . $item['name']);

        }
    }
}