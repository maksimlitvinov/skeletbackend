<?php

namespace Kefir\Services\Templates;

use Fenom;
use Exception;
use Kefir\Helper\Log;

class Template
{
    /**
     * @var Fenom
     */
    public $fenom;

    public function __construct()
    {
        $this->getFenom();
    }


    /**
     * @param null $template
     * @param array $data
     * @return mixed|null|string
     */
    public function render($template = null, $data = [], $error = false)
    {
        $output = null;

        if ($template == null) {
            $output = $this->ajaxResponse(true,'',$data);
        } else {
            if (!preg_match('#\.tpl$#', $template)) {
                $template .= '.tpl';
            }

            if ($error) {
                $template = '/' . $template;
            } else {
                $template = ENV . '/' . $template;
            }

            $output = $this->fenom->fetch($template, $data);
        }

        return $output;
    }

    /**
     * Инициализация Fenom
     * @return bool
     */
    public function getFenom()
    {
        if (!$this->fenom) {
            try {
                if (!file_exists(PROJECT_CACHE_TEMPLATE_PATH)) {
                    mkdir(PROJECT_CACHE_TEMPLATE_PATH);
                }
                $this->fenom = Fenom::factory(PROJECT_TEMPLATES_PATH, PROJECT_CACHE_TEMPLATE_PATH, PROJECT_FENOM_OPTIONS);

                $extFenom = get_class_methods(new ExtFenom());

                // @TODO Реализовать получение расширений из пользовательских модулей

                foreach ($extFenom as $key => $value) {
                    $this->fenom->addAccessorSmart("$key", "Kefir\Services\Templates\ExtFenom::$value", Fenom::ACCESSOR_CALL);
                }
            }
            catch (Exception $e) {
                Log::log($e->getMessage());
                return false;
            }
        }
    }

    /**
     * Вывод ответа в установленном формате для всех Ajax запросов
     * @param bool $success
     * @param string $message
     * @param array $data
     * @return string
     */
    public function ajaxResponse($success = true, $message = '', array $data = array()) {
        $response = array(
            'success' => $success,
            'message' => $message,
            'data' => $data,
        );
        return json_encode($response);
    }
}