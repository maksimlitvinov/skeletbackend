<?php
/**
 * List System Routes
 * @var $this->router = new \Book\Modules\Router\Router()
 */

// Error
$this->router->add('404', '/404.html', 'Page404Controller:index');
$this->router->add('503', '/503.html', 'DisabledController:index');

// Cron
$this->router->add('cron', '/cron/(module:str)/(action:str)/', 'CronController:index');

/*$this->router->add('settings', '/settings/', 'SettingsController:index');
$this->router->add('settings-user-create', '/settings/users/create/', 'SettingsController:createUser');
$this->router->add('settings-user-edit', '/settings/users/edit/(id:int)', 'SettingsController:editUser');
$this->router->add('settings-user-delete', '/settings/users/delete/(id:int)', 'SettingsController:deleteUser');
$this->router->add('settings-user-add', '/settings/users/add/', 'SettingsController:addUser', 'POST');
$this->router->add('settings-user-update', '/settings/users/update/', 'SettingsController:updateUser', 'POST');

$this->router->add('statistic', '/stat/', 'StatisticController:index');

$this->router->add('user', '/user/', 'UserController:index');
$this->router->add('user-edit', '/user/edit/', 'UserController:edit');
$this->router->add('user-update', '/user/update/', 'UserController:update', 'POST');*/


