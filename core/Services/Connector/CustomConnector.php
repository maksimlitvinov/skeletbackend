<?php

namespace Kefir\Services\Connector;

use Kefir\DI\DI;

abstract class CustomConnector
{
    /**
     * @var DI
     */
    public $di;
    /**
     * @var array
     */
    public $data = [];

    /**
     * CustomConnector constructor.
     * @param DI $di
     * @param array $data
     */
    public function __construct(DI $di, array $data)
    {
        $this->di = $di;
        $this->data = $data;
    }

    /**
     * @param bool $success
     * @param string $message
     * @param array $data
     */
    public function output($success = true, $message = '', array $data = [])
    {
        Connector::ajaxResponse($success, $message, $data);
    }

    public function prepareAction($action, $type)
    {
        return Connector::prepareAction($action, $type);
    }
}