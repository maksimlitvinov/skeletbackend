<?php

namespace Kefir\Services\Connector;

use Kefir\DI\DI;

class Connector
{
    /**
     * @var DI
     */
    protected $di;

    /**
     * @var \Kefir\Services\Http\Request
     */
    protected $req;

    public $action;

    public $data = [];

    /**
     * Connector constructor.
     * @param DI $di
     * @param \Kefir\Services\Router\DispatchedRoute $routerDispatch
     */
    public function __construct(DI $di, $routerDispatch)
    {
        $this->di = $di;
        $this->req = $this->di->get('req');

        $this->data = $this->req->get($this->req->server['REQUEST_METHOD']);
        $this->action = $this->data['action'];
        if (empty($this->action)) {
            self::ajaxResponse(false, 'Не указан экшен');
        }

        // @TODO Реализовать проверку на существующие методы вне API
        //list($class, $action) = explode(':', $routerDispatch->getController(), 2);
    }

    public function initialization()
    {
        $file = '\Api\\' . self::prepareAction($this->action,'file');
        $obj = new $file($this->di, $this->data);
        $action = self::prepareAction($this->action,'action');
        self::ajaxResponse($obj->$action());
    }

    public static function prepareAction($action,$type)
    {

        $file = 'class';
        $ac = 'action';

        $action = explode('/', $action);

        if ($type == $file) {
            return ucfirst($action[0]);
        } else if ($type == $ac) {
            return $action[1];
        }

        return self::ajaxResponse(false, 'Неверно указан тип запроса. Доступные типы: "' . $file . '", "' . $ac . '".');
    }

    /**
     * Вывод ответа в установленном формате для всех Ajax запросов
     *
     * @param bool|true $success
     * @param string $message
     * @param array $data
     */
    public static function ajaxResponse($success = true, $message = '', array $data = []) {
        $response = array(
            'success' => $success,
            'message' => $message,
            'data' => $data,
        );
        exit(json_encode($response));
    }
}