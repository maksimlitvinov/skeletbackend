<?php

namespace Modules\UsersVk;

use Kefir\DI\DI;

class UsersVk
{

    protected $di;

    public $groupLink = 'https://api.vk.com/method/groups.getMembers?group_id=52364172&access_token=93d2819b5ba22f33048fde57cddaae9a34971906a313ca04b63800737e0604c65ce13b396009e16e765ad&v=5.9';

    public function __construct(DI $di)
    {

        $this->di = $di;

        // Проверяем возможность подключения
        $cl_otvet = curl_init($this->groupLink);
        curl_setopt($cl_otvet,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($cl_otvet,CURLOPT_HEADER,true);
        curl_setopt($cl_otvet,CURLOPT_NOBODY,true);
        curl_setopt($cl_otvet,CURLOPT_RETURNTRANSFER,true);
        // Получаем ответ
        $response = curl_exec($cl_otvet);
        curl_close($cl_otvet);
        if (!$response) {
            return 'VK не отвечает!';
        }
    }

    public function getUsersVk()
    {

        // Получаем список id участников группы
        $contents = json_decode(file_get_contents($this->groupLink));

        return $this->getUsersVkItems($contents);
    }

    protected function getUsersVkItems($contents)
    {

        if(!is_string($contents)) {
            $idVkUsers = implode(',',$contents->response->items);
        }else {
            $idVkUsers = $contents;
        }

        $getListVkUsers = 'oauth=1&method=users.get&user_ids='.$idVkUsers.'&fields=maiden_name,photo_100&access_token=93d2819b5ba22f33048fde57cddaae9a34971906a313ca04b63800737e0604c65ce13b396009e16e765ad&v=5.9';
        //Получаем инфу о всех пользователях
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.vk.com/api.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $getListVkUsers);
        $vkuser = json_decode(curl_exec($curl), true);

        $vkusers = $vkuser['response'];

        return $vkusers;
    }
}