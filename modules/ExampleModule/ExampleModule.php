<?php

namespace Modules\ExampleModules;

use Kefir\DI\DI;

class ExampleModule
{
    /**
     * @var DI
     */
    protected $di;

    public function __construct(DI $di)
    {
        $this->di = $di;

        return 'Example ajaxAPI and Module worked';
    }
}