<?php

namespace App\Controllers;

use Kefir\Services\Auth\Auth;

use Kefir\DI\DI;
use Kefir\Controllers\UserController as User;

abstract class CloseAppController extends BaseAppController
{
    /**
     * CloseAppController constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);

        if (!Auth::authorized()) {
            self::Redirect('/login/');
        }

        $this->currentUser = User::getUserForLogin(Auth::hasUser());
        // TODO Check auth and add in $this->currentUser
    }

    public function getPermissions()
    {
        // TODO Определение Заказчик/Подрядчик и получение прав
    }
}