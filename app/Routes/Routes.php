<?php

namespace App\Routes;

use Kefir\DI\DI;

class Routes
{
    /** @var DI */
    public $di;

    /** @var \Kefir\Services\Router\Router */
    public $router;

    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->router = $this->di->get('router');

        return $this;
    }

    public function getEnvRoutes()
    {
        $this->router->add('home', '/', 'IndexController:index');
        $this->router->add('login', '/login/', 'LoginController:index');
        $this->router->add('auth', '/auth/', 'LoginController:login', 'POST');
        $this->router->add('logout', '/logout/', 'LoginController:logout');
        $this->router->add('profile', '/profile/', 'ProfileController:index');
        $this->router->add('profile-edit', '/profile/edit/', 'ProfileController:edit');
        $this->router->add('profile-update', '/profile/update/', 'ProfileController:update', 'POST');
    }
}