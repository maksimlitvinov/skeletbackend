jQuery(function($) {
    $('.ui.dropdown').dropdown('refresh');

    /** Uploads avatar on profile **/
    var fileExtentionRange = '.jpg .jpeg .png .JPG .gif .bmp';
    var MAX_SIZE = 3; // MB

    $(document).on('change', '.btn-file :file', function(e) {
        var input = $(this);

        if (navigator.appVersion.indexOf("MSIE") != -1) { // IE
            var label = input.val();

            input.trigger('fileselect', [ 1, label, 0 ]);
        } else {
            var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            var numFiles = input.get(0).files ? input.get(0).files.length : 1;
            var size = input.get(0).files[0].size;
            var file = input.get(0).files;

            input.trigger('fileselect', [ numFiles, label, size, file ]);
        }
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label, size, file) {
        $('#attachmentName').attr('name', 'attachmentName'); // allow upload.

        var postfix = label.substr(label.lastIndexOf('.'));
        if (fileExtentionRange.indexOf(postfix.toLowerCase()) > -1) {
            if (size > 1024 * 1024 * MAX_SIZE ) {
                alert('max size：<strong>' + MAX_SIZE + '</strong> MB.');

                $('#attachmentName').removeAttr('name'); // cancel upload file.
            } else {
                $('#_attachmentName').val(label);
                var reader = new FileReader();
                reader.onload = (function(theFile) {
                    return function (e) {
                        $('.avatar-tmp img').attr({
                            src: e.target.result,
                            alt: theFile.name
                        });
                    }
                    /*return function (e) {
                        $("<span>", {
                            append: $("<img>", {
                                src: e.target.result,
                                alt: theFile.name,
                            }),
                        }).appendTo('.avatar-tmp');
                    }*/
                })(file);
                reader.readAsDataURL(file[0]);
            }
        } else {
            alert('file type：<br/> <strong>' + fileExtentionRange + '</strong>');

            $('#attachmentName').removeAttr('name'); // cancel upload file.
        }
    });
    /** End Uploads avatar on profile **/

    /** Табы в настройках **/
    $('.menu .item').tab();

    /** Чекбокс партнера в форме пользователя **/
    var $partner = $('input[type="checkbox"][name="partner_id"]');
    if($partner.prop('checked')) {
        $('select[name="doljnost_id"]').val(5);
        $('select[name="doljnost_id"] option').each(function () {
            if($(this).val() != 5) {
                $(this).prop('disabled', true);
            }else {
                $(this).prop('disabled', false);
            }
        });
    }else{
        $('select[name="doljnost_id"] option').each(function () {
            if($(this).val() == 5  || $(this).val() == 0) {
                $(this).prop('disabled', true);
            }else{
                $(this).prop('disabled', false);
            }
        });
    }
    $partner.on('change', function () {
        if($(this).prop('checked')) {
            $('select[name="doljnost_id"]').val(5);
            $('select[name="doljnost_id"] option').each(function () {
                if($(this).val() != 5) {
                    $(this).prop('disabled', true);
                }else {
                    $(this).prop('disabled', false);
                }
            });
        }else {
            $('select[name="doljnost_id"] option').each(function () {
                if($(this).val() == 5  || $(this).val() == 0) {
                    $(this).prop('disabled', true);
                }else{
                    $(this).prop('disabled', false);
                }
            });
            $('select[name="doljnost_id"]').val(0);
        }

    })

    // Datapicker
    var today = new Date();
    $('#datepicker').calendar({
        type: 'date',
        firstDayOfWeek: 1,
        monthFirst: false,
        text: {
            days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Сегодня',
            now: 'Сейчас',
            am: 'AM',
            pm: 'PM'
        }
        //minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
    });

    // TimePicker
    $('#timepicker').calendar({
        type: 'time',
        ampm: false
    });


    // Форма вызова
    var visovForm = {

        elements: {},

        init: function () {
            this.startForm();
            this.changeWho();
            this.changeVk();
        },

        startForm: function () {
            var self = this;

            // Выбор юрика или физика
            if($('select[name="who"]').val() == 1) {
                $('.urik').removeClass('hidden').addClass('visible');
                $('.fisik').removeClass('visible').addClass('hidden');
            }

            // Участник группы VK
            if($('input[type="checkbox"][name="invk"]').prop('checked')){
                $('.vk').removeClass('hidden').addClass('visible');
                $('.notvk').removeClass('visible').addClass('hidden');
            }
        },

        changeWho: function () {
            var self = this;

            $('form.visovi').on('change', 'select[name="who"]', function() {
                if($(this).val() == 0){
                    $('.urik').removeClass('visible').addClass('hidden');
                    $('.fisik').removeClass('hidden').addClass('visible');
                }else{
                    $('.urik').removeClass('hidden').addClass('visible');
                    $('.fisik').removeClass('visible').addClass('hidden');
                }
            });
        },

        changeVk: function () {
            var self = this;

            $('form.visovi').on('change', 'input[type="checkbox"][name="invk"]', function () {
                if($('input[type="checkbox"][name="invk"]').prop('checked')){
                    $('.vk').removeClass('hidden').addClass('visible');
                    $('.notvk').removeClass('visible').addClass('hidden');
                }else{
                    $('.vk').removeClass('visible').addClass('hidden');
                    $('.notvk').removeClass('hidden').addClass('visible');
                }
            });
        }
    };
    visovForm.init();
});