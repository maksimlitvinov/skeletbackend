{extends '/App/Base.tpl'}

{block 'header'}{/block}

{block 'sidebar'}{/block}

{block 'template'}
    <h1>Сайт на техническом обслуживании</h1>
    <p>Заголовок: {$pagetitle}</p>
    <p>URI для Assets: {$assets_url}</p>
    <p>Путь для Assets: {$assets_path}</p>
    <p>URI сайта: {$site_url}</p>
    <p>Название сайта: {$site_name}</p>
    <p>Название сайта маленькими буквами: {$site_name_lower}</p>
{/block}

{block 'footer'}{/block}