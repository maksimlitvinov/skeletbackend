<div class="ui segment">
    <a href="{$user.backLink}" title="Вернуться назад" class="circular ui icon button">
        <i class="icon arrow left"></i>
    </a>
</div>
{if $messages}
    <div class="ui bottom attached warning message"><i class="icon warning sign"></i> {$messages} </div>
{/if}
<form action="{$action}" method="post" class="ui form" enctype="multipart/form-data">

    <input type="hidden" name="id" value="{$user.id}">

    <div class="field">

        <div class="ui four column grid">
            <div class="row">
                <div class="column">
                    <div class="avatar-tmp ui image">
                        <span><img src="{$user.image ?: $assets_url ~ 'images/logo.png'}"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui four column grid">
            <div class="row">
                <div class="column">
                    <div class="ui action input">
                        <input type="text" id="_attachmentName" value="{$user.image}" placeholder="Выберите изображение">
                        <label for="attachmentName" class="ui icon button btn-file">
                            <i class="attach basic icon"></i>
                            <input type="file" id="attachmentName" name="attachmentName" style="display: none">
                        </label>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="field">
        <label>Логин</label>
        <input type="text" name="login" placeholder="Логин" value="{$user.login}">
    </div>
    <div class="field">
        <label>Является партнером?</label>
        <div class="ui toggle checkbox">
            <input {$user.partner_id ? 'checked' : ''} type="checkbox" name="partner_id">
            <label></label>
        </div>
    </div>
    <div class="field">
        <label>Должность</label>
        <select name="doljnost_id">
            <option disabled value="0">Выберите должность</option>
            {foreach $.getDoljnost() as $doljnost}
                <option {$doljnost.id == $user.doljnost_id ? 'selected' : ''} value="{$doljnost.id}">{$doljnost.doljnost}</option>
            {/foreach}
        </select>
    </div>
    <div class="field">
        <label>Имя</label>
        <input type="text" name="user_name" placeholder="Имя" value="{$user.user_name}">
    </div>
    <div class="field">
        <label>Фамилия</label>
        <input type="text" name="last_name" placeholder="Фамилия" value="{$user.last_name}">
    </div>
    <div class="field">
        <label>Отчество</label>
        <input type="text" name="otchestvo" placeholder="Отчество" value="{$user.otchestvo}">
    </div>
    <div class="field">
        <label>Телефон</label>
        <input type="text" name="phone" placeholder="Телефон" value="{$user.phone}">
    </div>
    <div class="field">
        <label>E-mail</label>
        <input type="text" name="email" placeholder="E-mail" value="{$user.email}">
    </div>
    <div class="field">
        <label>Пароль</label>
        <input type="password" name="pass" placeholder="Пароль">
    </div>
    <div class="field">
        <label>Подтверждение пароля</label>
        <input type="password" name="confirmPass" placeholder="Подтверждение пароля">
    </div>
    <div class="field">
        <button class="ui basic button"><i class="icon user"></i> Сохранить</button>
    </div>
</form>