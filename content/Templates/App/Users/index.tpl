{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>
        <div class="ui segment">
            <div class="ui four column grid">
                <div class="row">
                    <div class="column">
                        <div class="avatar-tmp ui image">
                            <span><img src="{$user.image ?: $assets_url ~ 'images/logo.png'}"></span>
                        </div>
                    </div>
                    <div class="column">
                        <a href="/user/edit/" class="ui button">Редактировать</a>
                    </div>
                </div>
            </div>
            <table class="ui table">
                <tbody>
                    <tr>
                        <td>Ф.И.О.</td>
                        <td>{$user.last_name} {$user.user_name} {$user.otchestvo}</td>
                    </tr>
                    <tr>
                        <td>Логин</td>
                        <td>{$user.login}</td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td>{$user.phone}</td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td>{$user.email}</td>
                    </tr>
                    {if $user.partner_id}
                        <tr>
                            <td>Партнерский номер</td>
                            <td>{$user.partner_id}</td>
                        </tr>
                    {/if}
                </tbody>
            </table>
        </div>
    </div>
{/block}