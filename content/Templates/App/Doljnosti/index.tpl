{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>

        <div class="ui segment">
            <a href="/doljnosti/create/" class="ui green button">Добавить должность</a>
        </div>

        <div class="ui segment">
            <h2>Активные</h2>
            <table class="ui striped celled compact table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Должность</th>
                        <th>Ставка</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $data as $item}
                        <tr>
                            <td>{$item.id}</td>
                            <td>{$item.doljnost}</td>
                            <td>{$item.stavka}</td>
                            <td>
                                <div class="ui grid column">
                                    <a href="/doljnosti/edit/{$item.id}" title="Редактировать" class="column">
                                        <i class="edit icon"></i>
                                    </a>
                                    <a href="/doljnosti/delete/{$item.id}" class="column" title="Удалить">
                                        <i class="ban icon"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>

        {if $deleted}
            <div class="ui segment">
                <h2>Удаленные</h2>
                <table class="ui striped celled compact table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Должность</th>
                            <th>Ставка</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $deleted as $item}
                            <tr>
                                <td>{$item.id}</td>
                                <td>{$item.doljnost}</td>
                                <td>{$item.stavka}</td>
                                <td>
                                    <div class="ui grid column">
                                        <a href="/doljnosti/restore/{$item.id}" class="column" title="Восстановить">
                                            <i class="undo icon"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        {/if}

    </div>
{/block}