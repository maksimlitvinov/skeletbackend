<form action="/doljnosti/{$action}/" method="post" class="ui form">
    <input type="hidden" name="id" value="{$doljnost.id}">
    <div class="field">
        <div class="ui right labeled input">
            <label class="ui label">Название должности</label>
            <input type="text" name="doljnost" placeholder="Должность" value="{$doljnost.doljnost}">
        </div>
    </div>
    <div class="field">
        <div class="ui right labeled input">
            <label class="ui label">Ставка</label>
            <input type="text" name="stavka" placeholder="Укажите ставку" value="{$doljnost.stavka}">
            <div class="ui basic label">%, р. </div>
        </div>
    </div>
    <div class="field">
        <input type="submit" value="Сохранить" class="ui green button">
    </div>
</form>