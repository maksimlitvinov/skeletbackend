{extends 'App/Base.tpl'}

{block 'header'}{/block}

{block 'sidebar'}{/block}

{block 'template'}
    <h1 class="ui center aligned header">Вход в кабинет</h1>
    <div class="ui text container">
        <img class="ui centered image" src="{$assets_url}images/logo.png" alt="СЦ ПочиникА">
        {if $messages}
            <div class="ui bottom attached warning message"><i class="icon warning sign"></i> {$messages} </div>
        {/if}
        <div class="ui segment">
            <form action="/auth/" method="POST" class="ui form">
                <div class="field">
                    <label for="indexLogin">Ваш логин:</label>
                    <input id="indexLogin" type="text" name="login" placeholder="Введите Ваш логин">
                </div>
                <div class="field">
                    <label for="indexPass">Ваш пароль:</label>
                    <input id="indexPass" type="password" name="pass" placeholder="Введите Ваш пароль">
                </div>
                <input type="submit" class="ui basic button" value="Вход">
            </form>
        </div>
    </div>
{/block}

{block 'footer'}{/block}