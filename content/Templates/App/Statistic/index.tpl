{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>

        {if $data.visovi | length > 0}
            <div class="ui segment">
                <h4 class="ui dividing header">Вызовы</h4>
                <span>Всего вызовов: {$data.visovi_count}</span>
                <table class="ui table">
                    <thead>
                        <tr>
                            <th>More</th>
                            <th>ID</th>
                            <th>Дата</th>
                            <th>Время</th>
                            <th>Адрес</th>
                            <th>Проблема</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $data.visovi as $visov}
                            <tr>
                                <td>
                                    <a href="/visovi/more/{$visov.id}">
                                        <i class="id card icon"></i>
                                    </a>
                                </td>
                                <td>{$visov.id}</td>
                                <td>{$visov.dateforengineer | date : 'd-m-Y'}</td>
                                <td>{$visov.timeforengineer}</td>
                                {if $visov.adress}
                                    <td>{$visov.adres}</td>
                                {else}
                                    <td>{$visov.street}, {$visov.home}, {$visov.housing}, {$visov.apartment}</td>
                                {/if}
                                <td>{$visov.problemsk}</td>
                                <td>{$visov.status.name}</td>
                                <td>
                                    <a href="#" title="Отметить исполненным">
                                        <i class="flag checkered icon"></i>
                                    </a>
                                    {if $visov.status_id == 1}
                                        <a href="#" title="Отправить SMS">
                                            <i class="whatsapp icon"></i>
                                        </a>
                                    {/if}
                                    <a href="#" title="Добавить заметку">
                                        <i class="sticky note icon"></i>
                                    </a>
                                    <a href="/visovi/edit/{$visov.id}" title="Редактировать">
                                        <i class="edit icon"></i>
                                    </a>
                                    <a href="#" title="Удалить">
                                        <i class="ban icon"></i>
                                    </a>
                                    <a href="#" title="В черный список">
                                        <i class="warning sign icon"></i>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        {/if}

        {if $data.workshop | length > 0}
            <div class="ui segment">
                <h4 class="ui dividing header">Мастерская</h4>
            </div>
        {/if}
    </div>
{/block}