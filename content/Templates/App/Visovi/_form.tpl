{if $visov.backLink}
    <div class="ui segment">
        <a href="{$visov.backLink}" title="Вернуться назад" class="circular ui icon button">
            <i class="icon arrow left"></i>
        </a>
    </div>
{/if}
{if $messages}
    <div class="ui bottom attached warning message"><i class="icon warning sign"></i> {$messages} </div>
{/if}

<form class="ui form visovi" action="/visovi/{$action}/">
    <input type="hidden" name="id" value="{$visov.id}">
    <div class="ui segment">
        <h4 class="ui dividing header">Информация о вызове</h4>
        <div class="field">
            <label>Выберите партнера</label>
            <select class="ui fluid dropdown" name="partner">
                <option value="0">Выберите партнера</option>
                {foreach $.getPartners() as $partner}
                    <option {$visov.partner_id == $partner.partner_id ? 'selected' : ''} value="{$partner.partner_id}">{$partner.last_name} {$partner.user_name}</option>
                {/foreach}
            </select>
        </div>
        <div class="field">
            <label>Дата и время</label>
            <div class="two fields">
                <div class="field">
                    <div class="ui calendar" id="datepicker">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" name="dateforengineer" value="{$visov.dtfeng | date : 'd-m-Y'}">
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui calendar" id="timepicker">
                        <div class="ui input left icon">
                            <i class="time icon"></i>
                            <input type="text" name="timeforengineer" value="{$visov.dtfeng | date_format : '%H:%M'}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="field">
            <label>Назначить инжинера</label>
            <select name="engineer_id">
                <option value="0">Выберине инжинера</option>
                {foreach $.getEngeneer() as $engineer}
                    <option {$visov.engineer_id == $engineer.id ? 'selected' : ''} value="{$engineer.id}">{$engineer.user_name} {$engineer.last_name}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="ui segment">
        <h4 class="ui dividing header">Информация о клиенте</h4>
        <div class="field">
            <label>Телефон для связи</label>
            <input type="text" name="fone" value="{$visov.fone}">
        </div>
        <div class="field">
            <select class="ui fluid dropdown" name="who">
                <option value="0">Физическое лицо</option>
                <option {$visov.firm_id ? 'selected' : ''} value="1">Юридическое лицо</option>
            </select>
        </div>
        <div class="field urik hidden transition">
            <select name="urik" class="ui fluid search dropdown">
                <option value="0">Выберите организацию</option>
                {foreach $.getFirms() as $firm}
                    <option {$firm.id == $visov.firm_id ? 'selected' : ''}>{$firm.name}</option>
                {/foreach}
            </select>
        </div>

        <div class="fisik visible transition">
            <div class="field">
                <div class="ui toggle checkbox">
                    <input type="checkbox" name="invk" tabindex="0" {$visov.vkgroup ? 'checked' : ''}>
                    <label>Состоит в группе VK?</label>
                </div>
            </div>
            <div class="field vk hidden transition">
                <label>Участники группы VK.com</label>
                <div class="ui fluid search selection dropdown">
                    <input type="hidden" name="usersVk" value="{$visov.vkgroup}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Выберите участника</div>
                    <div class="menu">
                        {foreach $visov.usersVk as $user}
                            <div class="item" data-value="{$user.id}"><img class="ui mini avatar image" src="{$user.photo_100}"> {$user.first_name} {$user.last_name} </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="field notvk visible transition">
                <label for="">Имя заказавшего вызов</label>
                <input type="text" name="namek" value="{$visov.namek}">
            </div>
            <div class="fields">
                <div class="seven wide field">
                    <label>Улица</label>
                    <input type="text" name="street" value="{$visov.street}">
                </div>
                <div class="three wide field">
                    <label>Дом</label>
                    <input type="text" name="home" value="{$visov.home}">
                </div>
                <div class="six wide field">
                    <label>Корпус</label>
                    <input type="text" name="housing" value="{$visov.housing}">
                </div>
                <div class="six wide field">
                    <label>Квартира / Офис</label>
                    <input type="text" name="apartment" value="{$visov.apartment}">
                </div>
            </div>
        </div>
    </div>
    <div class="ui segment">
        <div class="field">
            <button class="ui basic button"><i class="icon newspaper"></i> Сохранить</button>
        </div>
    </div>
</form>