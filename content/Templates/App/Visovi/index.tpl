{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>
        <div class="segment">
            <a class="ui basic button" href="/visovi/create/">
                <i class="address card outline icon"></i>
                <span>Добавить вызов</span>
            </a>
        </div>
        <table class="ui celled table">
            <thead>
                <tr>
                    <th>More</th>
                    <th>ID</th>
                    <th>Дата</th>
                    <th>Время</th>
                    <th>Адрес</th>
                    <th>Проблема</th>
                    <th>Статус</th>
                </tr>
            </thead>
            <tbody>
                {foreach $visovi as $visov}
                    <tr>
                        <td>
                            <a href="/visovi/more/{$visov.id}">
                                <i class="newspaper icon"></i>
                            </a>
                        </td>
                        <td>{$visov.id}</td>
                        <td>{$visov.dateforengineer | date : 'd-m-Y'}</td>
                        <td>{$visov.timeforengineer}</td>
                        {if $visov.adress}
                            <td>{$visov.adres}</td>
                        {else}
                            <td>{$visov.street}, {$visov.home}, {$visov.housing}, {$visov.apartment}</td>
                        {/if}
                        <td>{$visov.problemsk}</td>
                        <td>{$visov.status.name}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
{/block}