{extends 'Base.tpl'}

{block 'template'}
    <div class="thirteen wide column">
        <h1>{$pagetitle}</h1>
        <a href="/firms/" title="К списку организаций" class="circular ui icon button">
            <i class="icon arrow left"></i>
        </a>
        <table class="ui striped table">
            <tbody>
                <tr>
                    <td>ID организации</td>
                    <td>{$firm.id}</td>
                </tr>
                <tr>
                    <td>Название</td>
                    <td>{$firm.name}</td>
                </tr>
                <tr>
                    <td>Телефон</td>
                    <td>{$firm.fone}</td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>{$firm.email}</td>
                </tr>
                <tr>
                    <td>Юридический индекс</td>
                    <td>{$firm.uindex}</td>
                </tr>
                <tr>
                    <td>Юридический адрес</td>
                    <td>{$firm.uadress}</td>
                </tr>
                <tr>
                    <td>Фактический адрес</td>
                    <td>{$firm.fadress}</td>
                </tr>
                <tr>
                    <td>ИНН</td>
                    <td>{$firm.inn}</td>
                </tr>
                <tr>
                    <td>КПП</td>
                    <td>{$firm.kpp}</td>
                </tr>
                <tr>
                    <td>ОКТМО</td>
                    <td>{$firm.oktmo}</td>
                </tr>
                <tr>
                    <td>Банк</td>
                    <td>{$firm.bank}</td>
                </tr>
                <tr>
                    <td>Р/С</td>
                    <td>{$firm.rs}</td>
                </tr>
                <tr>
                    <td>К/С</td>
                    <td>{$firm.ks}</td>
                </tr>
                <tr>
                    <td>БИК</td>
                    <td>{$firm.bik}</td>
                </tr>
                <tr>
                    <td>Директор</td>
                    <td>{$firm.fdir} {$firm.idir} {$firm.odir}</td>
                </tr>
                <tr>
                    <td>Основание</td>
                    <td>{$firm.osnov}</td>
                </tr>
                <tr>
                    <td>Договор</td>
                    <td>{$firm.dogovor}</td>
                </tr>
            </tbody>
        </table>
    </div>
{/block}