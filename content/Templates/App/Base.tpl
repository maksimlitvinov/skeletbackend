<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{$pagetitle}</title>
        <link rel="stylesheet" type="text/css" href="{$assets_url}libs/semantic/dist/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="{$assets_url}libs/semantic-ui-calendar/dist/calendar.min.css">
        <link rel="stylesheet" type="text/css" href="{$assets_url}css/custom.css">
    </head>
    <body>
        {block 'header'}
            <div class="ui fluid container"> {* Base block *}
                <div class="ui attached stackable menu">
                    <a class="item" href="/">Home</a>
                    <a class="item right" href="/settings/">Настройки</a>
                    <a class="item right" href="/user/">{$currentUser.user_name}</a>
                    <a class="item right" href="/logout/">Выход</a>
                </div>
                <div class="ui grid"> {* Base Grid *}
        {/block}

        {block 'sidebar'}
                    <div class="three wide column">
                        <div class="ui segment">

                            <h4 class="ui dividing header">Меню</h4>
                            <div class="ui vertical menu">
                                <a class="item" href="/visovi/">Вызовы <dvi class="ui {$currentUser.openVisoviCount > 0 ? 'teal' : ''} label">{$currentUser.openVisoviCount}</dvi></a>
                                <a class="item" href="/workshop/">Мастерская <dvi class="ui {$currentUser.openWorkCount > 0 ? 'teal' : ''} label">{$currentUser.openWorkCount}</dvi></a>
                                <a class="item" href="/avr/">АВР</a>
                                <a class="item" href="/sklad/">Склад</a>
                            </div>

                            <h4 class="ui dividing header">За {time() | date_format : '%B'}</h4>
                            <div class="ui vertical menu">
                                <div class="item">
                                    <p class="header">Вызовы</p>
                                    <div class="menu">
                                        <span class="item">Всего: <dvi class="ui label">{$.getCurrentVisoviBetweenCount($currentUser.id)} шт.</dvi></span>
                                        <span class="item">Заработано: <dvi class="ui label">{$.getCurrentVisoviBetweenPrice($currentUser.id)} р.</dvi></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        {/block}

        {block 'template'}{/block}

        {block 'footer'}
                </div> {* End Base Grid *}
            </div> {* End Base block *}
        {/block}

        <script src="{$assets_url}libs/jquery/dist/jquery.min.js"></script>
        <script src="{$assets_url}libs/semantic/dist/semantic.min.js"></script>
        <script src="{$assets_url}libs/semantic-ui-calendar/dist/calendar.min.js"></script>
        <script src="{$assets_url}js/custom.js"></script>

    </body>
</html>