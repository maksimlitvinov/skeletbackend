<?php

namespace Admin\Routes;

use Kefir\DI\DI;

class Routes
{
    /** @var DI */
    public $di;

    /** @var \Kefir\Services\Router\Router */
    public $router;

    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->router = $this->di->get('router');

        return $this;
    }

    public function getEnvRoutes()
    {
        $this->router->add('admin', '/admin/', 'DashboardController:index');
        $this->router->add('adminLogin', '/admin/login/', 'LoginController:index');
        $this->router->add('adminAuth', '/admin/auth/', 'LoginController:login', 'POST');
        $this->router->add('adminLogout', '/admin/logout/', 'LoginController:logout');
    }
}