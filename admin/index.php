<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once '../vendor/autoload.php';

use Kefir\Core;
define('ENV', 'Admin');
$core = new Core();
$core->init();