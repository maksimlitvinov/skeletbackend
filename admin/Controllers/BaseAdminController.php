<?php

namespace Admin\Controllers;

use SP\Controllers\BaseController;
use SP\DI\DI;
use SP\Modules\Auth\Auth;
use SP\Controllers\UserController as User;

abstract class BaseAdminController extends BaseController
{
    public function __construct(DI $di)
    {
        parent::__construct($di);

        if (!Auth::authorized()) {
            self::Redirect('/admin/login/');
        }

        $this->currentUser = User::getUserForLogin(Auth::hasUser());
    }
}