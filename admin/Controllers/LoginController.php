<?php

namespace Admin\Controllers;

use Kefir\Controllers\LoginBaseController as Login;
use Kefir\Services\Auth\Auth;

class LoginController extends Login
{
    public function index()
    {
        if (Auth::hasUser()) {
            self::Redirect('/admin/');
        }

        echo $this->template->render('Login/index', $this->getDataToTemplate());
    }

    public function login()
    {

        if (!$fromForm = $this->req->getPost()) {
            self::Redirect('/admin/login/');
        }

        /*$user = User::getUserForLogin($fromForm['login']);

        if (empty($user)) {
            $this->errorLogin('CheckLogin', 'Неправильное имя пользователя или пароль.', 'Неверный логин: ' . $fromForm['login']);
        }

        if (self::checkErrorLogin($user)) {
            $this->errorLogin('CheckErrorLogin', 'Пользователь заблокирован. Обратитесь к администратору.', 'Пользователь заблокирован: ' . $fromForm['login']);
        }

        $pass = Auth::encryptPassword($fromForm['pass'], $user->salt);
        if ($pass != $user->pass) {
            $user->login_error = (int) $user->login_error + 1;
            User::uib(['id' => $user->id, 'login_error' => $user->login_error], '', false);
            $this->errorLogin('CheckPassword', 'Неправильное имя пользователя или пароль.', 'Неверный пароль: ' . $fromForm['pass']);
        }

        Auth::authorize($fromForm['login']);

        self::Redirect('/admin/');*/

        echo 'Логин контроллер админки. Метод login()';
    }

    public function logout()
    {
        Auth::unAuthorize();
        self::Redirect('/admin/');
    }
}